(function () {

    'use strict';

    angular.module('giftApp', []).filter('bycategory', function ($rootScope) {
        return function (gifts, categories) {
            var items = {
                noResults: false,
                categories: categories,
                filtered: []
            };

            console.log(categories)
            angular.forEach(gifts, function (value, key) {
                angular.forEach(value.categories, function (category, key) {

                    if(category === 'grabgo') {
                        if (items.categories['grabgo'] === true) {
                            angular.forEach(Object.keys(items.categories), function (value, key) {
                                items.categories[value] = false;
                            });
                            items.categories['grabgo'] = true;
                            items.filtered.push(value);
                        }
                    }
                    else {
                        if (items.categories[category] === true) {
                            if (items.filtered.indexOf(value) === -1) {
                                items.filtered.push(value);
                            }
                        }
                    }


                })
            });

            items.selectedCategories = 0;

            angular.forEach(Object.keys(items.categories), function (value, key) {
                if (items.categories[value] === true) {
                    items.selectedCategories += 1;
                }
            })

            if (items.filtered.length === 0) {
                if (items.selectedCategories === 0) {
                    $rootScope.$broadcast('filterResults', items);
                    return gifts;
                }
                else {
                    items.noResults = true;
                }
            }
            $rootScope.$broadcast('filterResults', items);
            return items.filtered;
        };
    });

})();