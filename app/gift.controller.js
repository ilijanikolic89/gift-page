(function () {

    'use strict';

    angular.module('giftApp').controller('GiftController', ["$scope", GiftController]);

    function GiftController($scope) {

        $scope.noResults = false;

        $scope.$on('filterResults', function (event, data) {
            $scope.noResults = data.noResults;
        })

        $scope.gifts = [
            {
                title:'Add some love',
                price: 30,
                image: '/assets/images/thumbs/t1.jpg',
                categories: ['family', 'engraving']
            },
            {
                title:'Show your connection',
                price: 65,
                image: '/assets/images/thumbs/t2.jpg',
                categories: ['love', 'hobbies']
            },
            {
                title:'Show your strength',
                price: 35,
                image: '/assets/images/thumbs/t3.jpg',
                categories: ['family', 'purpose']
            },
            {
                title:'Show your spirit',
                price: 40,
                image: '/assets/images/thumbs/t4.jpg',
                categories: ['hobbies']
            },
            {
                title:'Show your gratitude',
                price: 35,
                image: '/assets/images/thumbs/t5.jpg',
                categories: ['purpose', 'love']
            },
            {
                title:'Add Engraving',
                price: 35,
                image: '/assets/images/thumbs/t6.jpg',
                categories: ['grabgo']
            }
        ];

        $scope.categories = {
            'family': false,
            'love': false,
            'faith': false,
            'hobbies': false,
            'purpose': false,
            'engraving': false,
            'grabgo': false
        }

    };
})();
