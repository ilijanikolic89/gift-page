//List of necessary gulp plugins
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require("gulp-autoprefixer"),
    concat = require("gulp-concat"),
    livereload = require('gulp-livereload'),
    browserSync = require('browser-sync').create(),
    browserSyncSpa = require('browser-sync-spa'),
    historyFallback = require('connect-history-api-fallback');


browserSync.use(browserSyncSpa({
    selector: '[ng-app]'
}));

// Compile sass into CSS & auto-inject browsers prefixes
gulp.task('sass', function() {
    return gulp.src("./assets/scss/**/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("assets/css"))
});

gulp.task('sass-watch', ['sass'], function (done) {
    //browserSync.reload();
    //done();
});

gulp.task('default', ['sass'], function() {

    browserSync.init({
        server: {
            baseDir: './',
            middleware: [
                historyFallback()
            ]
        }
    });

    //livereload.listen();

    gulp.watch("./assets/scss/**/*.scss", ['sass-watch']);
});
